import { Onboarding } from "./src/screens/Onboarding/Onboarding";
import { useFonts } from "expo-font";
import AppLoading from "expo-app-loading";

export default function App() {
  let [fontsLoaded] = useFonts({
    PPPangram: require("./assets/fonts/PPPangramSansRounded-Bold.otf"),
  });

  if (!fontsLoaded) {
    return <AppLoading />;
  }
  return <Onboarding />;
}
