import React from "react";
import { Dimensions, ImageSourcePropType } from "react-native";
import { Image } from "react-native";
import { Text, View } from "react-native";
import styles from "./Slide.module.css";

interface SlideProps {
  heading: string;
  image: string;
}

export const Slide = ({ heading, image }: SlideProps) => {
  const windowHeight = Dimensions.get("window").height;
  const windowWidth = Dimensions.get("window").width;
  const randomTop = Math.floor(Math.random() * 200) + 1;
  const colors = ["#F3CDF3", "#4878E9", "#6DB798", "#F66A40", "#F2B758"];
  return (
    <View style={(styles.slideContainer, { width: windowWidth })}>
      <View style={{ height: windowHeight / 2 }}>
        <Image
          style={[styles.image, { resizeMode: "cover" }]}
          source={image as ImageSourcePropType}
        />
        <View
          style={{
            width: 150,
            height: 150,
            backgroundColor: colors[Math.floor(Math.random() * 4) + 1],
            position: "absolute",
            top: randomTop,
            left: -70,
            borderRadius: 100,
          }}
        />
      </View>
      <Text style={[styles.heading]}>{heading}</Text>
    </View>
  );
};
