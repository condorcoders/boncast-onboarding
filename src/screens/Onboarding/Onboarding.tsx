import React, { useState } from "react";
import { useRef } from "react";
import {
  Animated,
  Button,
  Dimensions,
  Pressable,
  ScrollView,
  Text,
  View,
} from "react-native";
import { Slide } from "../../components/Slide/Slide";
import styles from "./Onboarding.module.css";

const slides = [
  {
    heading: "Listen trends podcasts anytime, anywhere",
    image: require("../../../assets/onboarding1.png"),
  },
  {
    heading: "Interact with podcasts & Appreciate host",
    image: require("../../../assets/onboarding2.png"),
  },
  {
    heading: "Enjoy podcasts playlist based on your mood",
    image: require("../../../assets/onboarding3.png"),
  },
];
export const Onboarding = () => {
  const windowWidth = Dimensions.get("window").width;
  const scrollRef = useRef<ScrollView>(null);
  const [activeIndex, setActiveIndex] = useState(0);

  const updateIndex = (newIndex: number) => {
    if (newIndex >= slides.length) {
      newIndex = 0;
    }
    scrollRef.current?.scrollTo({
      x: newIndex * windowWidth,
      animated: true,
    });
    setActiveIndex(newIndex);
  };

  return (
    <View style={styles.container}>
      <ScrollView
        ref={scrollRef}
        horizontal
        scrollEventThrottle={32}
        scrollEnabled={false}
        showsHorizontalScrollIndicator={false}
        pagingEnabled={true}
      >
        {slides.map((slide) => (
          <Slide
            key={slide.heading}
            heading={slide.heading}
            image={slide.image}
          />
        ))}
      </ScrollView>
      <View style={styles.indicatorContainer}>
        {slides.map((_, i) => (
          <View
            key={i}
            style={[
              styles.indicator,
              {
                width: activeIndex === i ? 20 : 10,
                backgroundColor: activeIndex === i ? "#F66A40" : "#ccc3ba",
              },
            ]}
          />
        ))}
      </View>
      <View style={styles.buttonContainer}>
        <Pressable
          style={styles.button}
          onPress={() => {
            updateIndex(activeIndex + 1);
          }}
        >
          <Text style={styles.text}>Next</Text>
        </Pressable>
      </View>
    </View>
  );
};
